﻿module sfmld.internal.refcount;

private
{
    import std.format;
    import std.c.stdlib : malloc, free;

    import derelict.sfml2.audio, derelict.sfml2.graphics, derelict.sfml2.network, derelict.sfml2.system,
        derelict.sfml2.window;
}

/// Ref counted derelict-sfml pointer(Because most of them have a sfWhatever_destroy() function that should be used)
struct SfRefCounted(sfType)
{
    private
    {
        sfType* _handle;
        uint*   _count;
    }

    @nogc @trusted
    public nothrow
    {
        this(this)
        {
            if(this._count !is null)
            {
                *this._count += 1;
            }
        }

        /// Create a new ref count around the given handle.
        /// 
        /// Parameters:
        ///     handle = The handle to keep ref counted.
        this(sfType* handle)
        {
            this._handle = handle;
            this._count = cast(uint*)malloc(uint.sizeof);
        }

        ~this()
        {
            if(this._count is null)
            {
                return;
            }

            *this._count -= 1;
            if(*this._count == 0)
            {
                //pragma(msg, __traits(identifier, sfType));

                mixin(format("%s_destroy(this._handle);", __traits(identifier, sfType)));
                free(this._count);
            }
        }

        /// Get the handle that is being ref counted.
        @property
        auto handle() inout
        {
            return this._handle;
        }
    }
}