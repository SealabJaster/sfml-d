﻿module sfmld.system.clock;

private
{
    import derelict.sfml2.system;

    import sfmld.internal.refcount;
}

/// Helper struct for derelict's sfTime
struct Time
{
    /// Derelict's sfTime struct that this struct is helping around.
    sfTime  time;

    @property @nogc @trusted
    public nothrow const
    {
        /// Get the time in seconds.
        float seconds()
        {
            return sfTime_asSeconds(this.time);
        }

        /// Get the time in milliseconds.
        int milliseconds()
        {
            return sfTime_asMilliseconds(this.time);
        }

        /// Get the time in microseconds
        long microseconds()
        {
            return sfTime_asMicroseconds(this.time);
        }
    }
}

/// Used to measure time.
struct Clock
{
    private
    {
        SfRefCounted!sfClock    _handleRef;
    }

    @nogc @trusted
    public nothrow
    {
        /// Creates a clock around the given sfClock*.
        /// 
        /// Parameters:
        ///     handle = The sfClock to use. If null, then one is created.
        this(sfClock* handle)
        {
            assert(handle !is null);
            this._handleRef = SfRefCounted!sfClock(handle);
        }

        /// Creates a new clock.
        /// 
        /// Returns:
        ///     A new clock
        static Clock create()
        {
            return Clock(sfClock_create());
        }

        /// Creates a copy of the clock.
        /// 
        /// Returns:
        ///     A copy of the clock.
        Clock copy() inout
        {
            return Clock(sfClock_copy(this.handle));
        }

        /// Restarts the clock, returning the elapsed time.
        /// 
        /// Returns:
        ///     How much time has elapsed since the clock was created/last restarted.
        Time restart()
        {
            return Time(sfClock_restart(this.handle));
        }

        @property
        {
            /// NOTE - To prevent buggy issues, please only use this if you need to call a CSFML function.
            /// Get the sfClock* handle.
            auto handle() inout
            {
                return this._handleRef.handle;
            }

            /// Get how much time has elapsed since the last restart or since the clock was created.
            Time elapsedTime() inout
            {
                return Time(sfClock_getElapsedTime(this.handle));
            }
        }
    }
}

// TODO: Write tests, and overload those operators.