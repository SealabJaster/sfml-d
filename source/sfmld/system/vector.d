module sfmld.system.vector;

private
{
    import derelict.sfml2.system;
}

/// Determines whether the type is one of derelict's sfVector types.
bool isVector(T)()
{
    return (
        is(T == sfVector2f) ||
        is(T == sfVector2i) ||
        is(T == sfVector2u)
    );
}

/// A helper struct to make handling with derelict's vectors easier.
struct Vector(sfVector)
if(isVector!sfVector)
{
    /// The derelict struct that this struct is helping around.
    sfVector     vector;
    alias vector this; // Just so you can do Vector.x and Vector.y

    alias fieldType = typeof(sfVector.init.x);

    /// Create a new vector using the given x and y.
    /// 
    /// Parameters:
    ///     x = The vector's x
    ///     y = The vector's y
    this(fieldType x, fieldType y)
    {
        this.vector = sfVector(x, y);
    }

    // TODO: Overload operators(since that's the entire reason this struct exists) <3
    Vector!sfVector opBinary(string op)(Vector!sfVector vect)
    {
        return mixin("Vector!sfVector(this.vector.x "~op~" vect.x, this.vector.y "~op~" vect.y)");
    }

    void opOpAssign(string op)(Vector!sfVector vect)
    {
        mixin("this.vector.x "~op~"= vect.x;");
        mixin("this.vector.y "~op~"= vect.y;");
    }
}
unittest
{
    auto vect1 = Vector2i(20, 50);
    auto vect2 = Vector2i(5, 2);
    auto result = (vect1 * vect2);

    vect1 *= vect2;
    assert(vect1 == vect2);
}

/// Vector of floats.
alias Vector2f = Vector!sfVector2f;

/// Vector of ints.
alias Vector2i = Vector!sfVector2i;

/// Vector of unisnged ints.
alias Vector2u = Vector!sfVector2u;